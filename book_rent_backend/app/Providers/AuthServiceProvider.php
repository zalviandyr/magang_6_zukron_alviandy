<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function (Request $request) {
            $header = $request->header('Authorization');

            if ($header) {
                $basicAuth = explode(' ', $header)[1];
                $userPass = base64_decode($basicAuth);
                $username = explode(':', $userPass)[0];
                $password = explode(':', $userPass)[1];

                $user = User::where('email', $username)->first();
                if ($user != null && Hash::check($password, $user->password)) {
                    return $user;
                }
            }
        });
    }
}
