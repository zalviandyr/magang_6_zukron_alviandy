<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    public $timestamps = false;

    protected $fillable = [
        'genre_id', 'title', 'price', 'image', 'pemilik_id', 'peminjam_id', 'status', 'is_publish'
    ];

    public function genre()
    {
        return $this->hasOne(Genre::class, 'id', 'genre_id');
    }

    public function peminjam()
    {
        return $this->hasOne(User::class, 'id', 'peminjam_id');
    }

    public function pemilik()
    {
        return $this->hasOne(User::class, 'id', 'pemilik_id')->withDefault(null);
    }

    public function getImageUrlAttribute()
    {
        return url('image/book/' . $this->image);
    }
}
