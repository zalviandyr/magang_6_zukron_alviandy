<?php

namespace App\Http\Controllers\Peminjam;

use App\Helper\FirebaseHelper;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function getAll()
    {
        $books = Book::where('is_publish', true)
            ->where('status', 'tidak dipinjam')
            ->get();

        $data = [];
        foreach ($books as $book) {
            array_push($data, [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Success get book',
            'data' => $data
        ]);
    }

    public function getRentBook()
    {
        $user = Auth::user();
        $books = Book::where('is_publish', true)
            ->where('status', 'dipinjam')
            ->where('peminjam_id', $user->id)
            ->get();

        $data = [];
        foreach ($books as $book) {
            array_push($data, [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Success get book',
            'data' => $data
        ]);
    }

    public function rentBook($bookId)
    {
        $book = Book::where('id', $bookId)
            ->where('is_publish', true)
            ->where('status', 'tidak dipinjam')
            ->first();

        if ($book == null) {
            return response()->json([
                'success' => false,
                'message' => 'Book not found',
                'data' => null
            ], 404);
        }

        $user = Auth::user();
        $book->status = 'dipinjam';
        $book->peminjam_id = $user->id;
        $book->save();

        // send notification to pemilik
        $pemilik = User::where('id', $book->pemilik_id)
            ->first();
        FirebaseHelper::sendNotification($pemilik->device_id, 'Buku dipinjam', 'Ada buku yang dipinjam');

        return response()->json([
            'success' => true,
            'message' => 'Success rent book',
            'data' => [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]
        ]);
    }

    public function returnBook($bookId)
    {
        $book = Book::where('id', $bookId)
            ->where('is_publish', true)
            ->where('status', 'dipinjam')
            ->first();

        if ($book == null) {
            return response()->json([
                'success' => false,
                'message' => 'Book not found',
                'data' => null
            ], 404);
        }

        $book->status = 'tidak dipinjam';
        $book->peminjam_id = null;
        $book->save();

        // send notification to pemilik
        $pemilik = User::where('id', $book->pemilik_id)
            ->first();
        FirebaseHelper::sendNotification($pemilik->device_id, 'Buku dikembalikan', 'Ada buku yang dikembalikan');

        return response()->json([
            'success' => true,
            'message' => 'Success return book',
            'data' => [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]
        ]);
    }
}
