<?php

namespace App\Http\Controllers\Pemilik;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function getAll()
    {
        $user = Auth::user();
        $books = Book::where('pemilik_id', $user->id)
            ->get();

        $data = [];
        foreach ($books as $book) {
            array_push($data, [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Success get book',
            'data' => $data
        ]);
    }
    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'genre_id' => 'required',
            'title' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|image',
            'is_publish' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
                'data' => null,
            ], 422);
        }

        $genre = Genre::find($request->genre_id);
        if ($genre == null) {
            return response()->json([
                'success' => false,
                'message' => 'Genre not found',
                'data' => null,
            ], 404);
        }

        // upload image
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $fileName = urlencode(uniqid('book-' . $request->title . '-')) . '.' . $ext;
        $image->storeAs('image/book', $fileName);

        $user = Auth::user();
        $book = Book::create([
            'genre_id' => $genre->id,
            'title' => $request->title,
            'price' => $request->price,
            'image' => $fileName,
            'pemilik_id' => $user->id,
            'status' => 'tidak dipinjam',
            'is_publish' => $request->is_publish,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Success create book',
            'data' => [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'genre_id' => 'required',
            'title' => 'required',
            'price' => 'required|numeric',
            'is_publish' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
                'data' => null,
            ], 422);
        }

        $genre = Genre::find($request->genre_id);
        if ($genre == null) {
            return response()->json([
                'success' => false,
                'message' => 'Genre not found',
                'data' => null,
            ], 404);
        }

        $book = Book::find($id);
        if ($book == null) {
            return response()->json([
                'success' => false,
                'message' => 'Book not found',
                'data' => null,
            ], 404);
        }

        $book->genre_id = $request->genre_id;
        $book->title = $request->title;
        $book->price = $request->price;
        $book->is_publish = $request->is_publish;
        $book->save();

        return response()->json([
            'success' => true,
            'message' => 'Success update book',
            'data' => [
                'id' => $book->id,
                'title' => $book->title,
                'price' => $book->price,
                'price' => $book->price,
                'image' => $book->image_url,
                'genre' => $book->genre,
                'price_desc' => 'Rp. ' . number_format($book->price),
                'status' => $book->status,
                'is_publish' => $book->is_publish,
                'pemilik' => $book->pemilik,
                'peminjam' => $book->peminjam,
            ]
        ], 201);
    }

    public function delete($id)
    {
        $book = Book::find($id);
        if ($book == null) {
            return response()->json([
                'success' => false,
                'message' => 'Book not found',
                'data' => null,
            ], 404);
        }

        $book->delete();

        return response()->json([
            'success' => true,
            'message' => 'Success delete book',
            'data' => null,
        ]);
    }
}
