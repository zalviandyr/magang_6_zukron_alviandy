<?php

namespace App\Http\Controllers;

use App\Models\Genre;

class GenreController extends Controller
{
    public function getAll()
    {
        $genres = Genre::all();

        return response()->json([
            'success' => true,
            'message' => 'Success get genre',
            'data' => $genres,
        ]);
    }
}
