<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PemilikMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->role->role == 'pemilik') {
            return $next($request);
        }

        return response()->json([
            'success' => false,
            'message' => 'Unauthorized',
            'data' => null
        ], 401);
    }
}
