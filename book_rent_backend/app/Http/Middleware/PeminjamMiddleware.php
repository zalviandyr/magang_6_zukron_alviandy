<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PeminjamMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->role->role == 'peminjam') {
            return $next($request);
        }

        return response()->json([
            'success' => false,
            'message' => 'Unauthorized',
            'data' => null
        ], 401);
    }
}
