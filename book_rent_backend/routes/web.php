<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// image
$router->get('/image/{path}/{filename}', ['uses' => 'ImageController@image']);

// auth
$router->post('/register', ['uses' => 'AuthController@register']);
$router->post('/login', ['uses' => 'AuthController@login']);

// genre
$router->group(['middleware' => ['auth'], 'prefix' => 'genre'], function () use ($router) {
    $router->get('/', ['uses' => 'GenreController@getAll']);
});

// pemilik
$router->group(['middleware' => ['auth', 'pemilik'], 'namespace' => 'Pemilik', 'prefix' => 'pemilik'], function () use ($router) {
    // book
    $router->group(['prefix' => 'book'], function () use ($router) {
        $router->get('/', ['uses' => 'BookController@getAll']);
        $router->post('/', ['uses' => 'BookController@insert']);
        $router->patch('/{id}', ['uses' => 'BookController@update']);
        $router->delete('/{id}', ['uses' => 'BookController@delete']);
    });
});

// peminjam
$router->group(['middleware' => ['auth', 'peminjam'], 'namespace' => 'Peminjam', 'prefix' => 'peminjam'], function () use ($router) {
    // book
    $router->group(['prefix' => 'book'], function () use ($router) {
        $router->get('/', ['uses' => 'BookController@getAll']);
        $router->get('/rent', ['uses' => 'BookController@getRentBook']);
        $router->patch('/rent/{bookId}', ['uses' => 'BookController@rentBook']);
        $router->patch('/return/{bookId}', ['uses' => 'BookController@returnBook']);
    });
});
