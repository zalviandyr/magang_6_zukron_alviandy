<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    public function run()
    {
        DB::table('genres')->insert(['genre' => 'Romance']);
        DB::table('genres')->insert(['genre' => 'Fanfiction']);
        DB::table('genres')->insert(['genre' => 'Science Fiction']);
        DB::table('genres')->insert(['genre' => 'Fantasy']);
        DB::table('genres')->insert(['genre' => 'Thriller ']);
        DB::table('genres')->insert(['genre' => 'Historical']);
        DB::table('genres')->insert(['genre' => 'Horror']);
    }
}
