<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('genre_id')->unsigned();
            $table->string('title');
            $table->integer('price');
            $table->string('image');
            $table->bigInteger('pemilik_id')->unsigned();
            $table->bigInteger('peminjam_id')->unsigned()->nullable();
            $table->enum('status', ['dipinjam', 'tidak dipinjam']);
            $table->boolean('is_publish');

            $table->foreign('genre_id')->on('genres')->references('id');
            $table->foreign('pemilik_id')->on('users')->references('id');
            $table->foreign('peminjam_id')->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
