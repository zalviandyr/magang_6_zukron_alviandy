String LetterChanges(String str) {
  List<String> vowels = ['a', 'i', 'u', 'e', 'o'];
  List<String> list = str.split('').map((e) {
    String charLower = e.toLowerCase();
    if (RegExp(r'[a-z]').hasMatch(charLower)) {
      int runes = charLower.runes.first + 1;
      String char = String.fromCharCode(runes);
      if (vowels.contains(char)) {
        return char.toUpperCase();
      }
      return char;
    }
    return charLower;
  }).toList();

  return list.join('');
}

void main() {
  print(LetterChanges('hello*3'));
  print(LetterChanges('fun times!'));
}
