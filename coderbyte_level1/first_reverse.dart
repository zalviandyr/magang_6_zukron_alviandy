String FirstReverse(String str) => str.split('').reversed.join('');

void main() {
  print(FirstReverse('alvin'));
}
