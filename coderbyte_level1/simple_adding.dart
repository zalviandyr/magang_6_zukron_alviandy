int SimpleAdding(int num) {
  int result = 0;
  for (int i = 1; i <= num; i++) {
    result += i;
  }
  return result;
}

// keep this function call here
void main() {
  print(SimpleAdding(12));
  print(SimpleAdding(140));
}
