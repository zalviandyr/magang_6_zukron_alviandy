String LongestWord(String sen) {
  List<Map<String, int>> list = sen
      .split(' ')
      .map((e) => e
          .split('')
          .where((element) => RegExp(r'[a-zA-Z0-9]').hasMatch(element))
          .join(''))
      .map((e) => {e: e.length})
      .toList()
        ..sort((a, b) => b.values.first.compareTo(a.values.first));

  return list.first.keys.first;
}

void main() {
  print(LongestWord('fun&!! time'));
  print(LongestWord('i love dogs'));
  print(LongestWord('123456789 98765432'));
}
