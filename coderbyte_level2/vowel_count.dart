int VowelCount(String str) {
  List<String> vowels = ['a', 'i', 'u', 'e', 'o'];

  return str
      .split('')
      .where((element) => vowels.contains(element))
      .toList()
      .length;
}

// keep this function call here
void main() {
  print(VowelCount('hello'));
  print(VowelCount('coderbyte'));
}
