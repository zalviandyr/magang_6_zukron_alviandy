String CheckNums(int num1, int num2) {
  if (num1 > num2) return 'false';
  if (num1 < num2) return 'true';
  return '-1';
}

// keep this function call here
void main() {
  print(CheckNums(3, 122));
  print(CheckNums(67, 67));
}
