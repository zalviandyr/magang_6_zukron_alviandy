String SimpleSymbols(String str) {
  String result = 'true';
  List<String> list = str.split('');

  for (int i = 0; i < list.length; i++) {
    if (RegExp(r'[a-zA-Z]').hasMatch(list[i])) {
      if (i > 0) {
        if (list[i - 1] != '+' || list[i + 1] != '+') {
          result = 'false';
        }
      } else {
        result = 'false';
      }
    }
  }

  return result;
}

// keep this function call here
void main() {
  print(SimpleSymbols('+d+=3=+s+'));
  print(SimpleSymbols('f++d+'));
}
