String AlphabetSoup(String str) {
  List runes = str.runes.toList()..sort();
  return runes.map((e) => String.fromCharCode(e)).toList().join('');
}

// keep this function call here
void main() {
  print(AlphabetSoup('coderbyte'));
  print(AlphabetSoup('hooplan'));
}
