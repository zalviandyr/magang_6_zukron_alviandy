String ABCheck(String str) {
  String result = 'false';
  List<String> list = str.toLowerCase().split('');
  for (int i = 0; i < list.length; i++) {
    if (list[i] == 'a') {
      int before = i - 4;
      int after = i + 4;
      if (after <= list.length - 1) {
        if (list[after] == 'b') result = 'true';
      }
      if (before >= 0) {
        if (list[before] == 'b') result = 'true';
      }
    }
  }

  return result;
}

void main() {
  print(ABCheck('after badly'));
  print(ABCheck('laura sobs'));
  print(ABCheck('bzzza'));
  print(ABCheck('azzzb'));
}
