String LetterCapitalize(String str) => str
    .split(' ')
    .map((e) => e[0].toUpperCase() + e.substring(1, e.length))
    .join(' ');

// keep this function call here
void main() {
  print(LetterCapitalize('hello world'));
  print(LetterCapitalize('i ran there'));
}
