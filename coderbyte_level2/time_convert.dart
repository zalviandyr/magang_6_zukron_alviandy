String TimeConvert(int num) {
  int hours = num ~/ 60;
  int minutes = num % 60;
  return '$hours:$minutes';
}

// keep this function call here
void main() {
  print(TimeConvert(126));
  print(TimeConvert(45));
}
