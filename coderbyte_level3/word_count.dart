int WordCount(String str) => str.split(' ').length;

// keep this function call here
void main() {
  print(WordCount('Hello World'));
  print(WordCount('one 22 three'));
}
