String ExOh(String str) {
  String result = 'false';
  int x = str.split('').where((element) => element == 'x').length;
  int o = str.split('').where((element) => element == 'o').length;

  if (x == o) result = 'true';

  return result;
}

// keep this function call here
void main() {
  print(ExOh('xooxxo'));
  print(ExOh('x'));
}
