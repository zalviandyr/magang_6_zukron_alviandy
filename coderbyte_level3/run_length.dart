String RunLength(String str) {
  List<Map<String, int>> list = str.split('').map((e) => {e: 1}).toList();

  for (int i = 0; i < list.length; i++) {
    int after = i + 1;
    if (after < list.length) {
      if (list[i].keys.first == list[after].keys.first) {
        list[i][list[i].keys.first] = list[i].values.first + 1;

        list.removeAt(after);
        i--;
      }
    }
  }

  List<String> result = [];
  for (var item in list) {
    result.add(item.values.first.toString());
    result.add(item.keys.first);
  }

  return result.join('');
}

void main() {
  print(RunLength('aaabbcde'));
  print(RunLength('wwwbbbw'));
}
