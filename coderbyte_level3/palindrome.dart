String Palindrome(String str) {
  List list = str.replaceAll(' ', '').split('');
  String normal = list.join('');
  String reverse = list.reversed.join('');

  return normal == reverse ? 'true' : 'false';
}

// keep this function call here
void main() {
  print(Palindrome('never odd or even'));
  print(Palindrome('eye'));
}
