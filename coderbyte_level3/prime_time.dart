import 'dart:math';

String PrimeTime(int num) {
  List firstPrime = [1, 2, 3, 5];
  if (num >= 1 && num <= pow(2, 16)) {
    if (firstPrime.contains(num)) return 'true';

    if (num % 2 == 1 && num % 3 != 0 && num % 5 != 0) {
      return 'true';
    }

    return 'false';
  } else {
    return 'false';
  }
}

void main() {
  print(PrimeTime(19));
  print(PrimeTime(110));
  print(PrimeTime(65536));
  print(PrimeTime(65537));
}
