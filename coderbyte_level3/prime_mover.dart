import 'dart:math';

int PrimeMover(int num) {
  int max = pow(10, 4).toInt();
  List<bool> primes = [];
  for (int i = 0; i <= max; i++) {
    primes.add(true);
  }

  for (int i = 2; i <= sqrt(max); i++) {
    if (primes[i]) {
      for (int j = i * i; j <= max; j += i) {
        primes[j] = false;
      }
    }
  }

  List<int> resultPrime = [];
  for (int i = 2; i <= max; i++) {
    if (primes[i]) resultPrime.add(i);
  }

  return resultPrime[num - 1];
}

void main() {
  print(PrimeMover(9));
  print(PrimeMover(100));
}
