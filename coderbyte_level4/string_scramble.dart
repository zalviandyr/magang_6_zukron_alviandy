String StringScramble(String str1, String str2) {
  List list1 = str1.split('').toList()..sort();
  List list2 = str2.split('').toList()..sort();

  for (int i = 0; i < list2.length; i++) {
    for (int j = 0; j < list1.length; j++) {
      if (list2[i] == list1[j]) {
        list2.removeAt(i);
        i--;

        list1.removeAt(j);
        j--;
        break;
      }
    }
  }

  return list2.length > 0 ? 'false' : 'true';
}

// keep this function call here
void main() {
  print(StringScramble('rkqodlw', 'world'));
  print(StringScramble('cdore', 'coder'));
  print(StringScramble('h3llko', 'hello'));
  print(StringScramble('win33er', 'winner'));
}
