String TransitivityRelations(List strArr) {
  List<List<int>> matrix = [];
  for (var item in strArr) {
    String str = item;
    List<int> temp = str
        .substring(1, str.length - 1)
        .split(',')
        .map((e) => int.parse(e))
        .toList();

    matrix.add(temp);
  }

  List<String> result = [];
  for (int row = 0; row < matrix.length; row++) {
    for (int col = 0; col < matrix.length; col++) {
      if (row != col && matrix[row][col] == 1) {
        checkConnection(row, col, matrix, result);
      }
    }
  }

  if (result.length == 0) {
    return 'transitive';
  } else {
    result.sort();
    return result.join('-');
  }
}

void checkConnection(
    int curNode, int nextNode, List<List<int>> matrix, List<String> result) {
  for (int i = 0; i < matrix[nextNode].length; i++) {
    if (matrix[nextNode][i] == 1 && curNode != i && matrix[curNode][i] == 0) {
      matrix[curNode][i] = 1;
      result.add('($curNode,$i)');
    }
  }
}

void main() {
  // print(TransitivityRelations(["(1,1,1)", "(0,1,1)", "(0,1,1)"]));
  print(
      TransitivityRelations(["(1,1,1)", "(1,0,0)", "(0,1,0)"])); // (1,2)-(2,0)
  print(TransitivityRelations([
    "(1,1,0,0)",
    "(0,0,1,0)",
    "(0,1,0,1)",
    "(1,0,0,1)"
  ])); // (0,2)-(0,3)-(1,0)-(1,3)-(2,0)-(3,1)-(3,2)
}
