int Division(int num1, int num2) {
  while (num2 != 0) {
    int temp = num2;
    num2 = num1 % num2;
    num1 = temp;
  }

  return num1;
}

void main() {
  print(Division(12, 16));
  // print(Division(7, 3));
  // print(Division(36, 54));
}
