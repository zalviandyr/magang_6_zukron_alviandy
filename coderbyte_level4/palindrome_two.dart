String PalindromeTwo(String str) {
  List<String> list = str
      .split('')
      .where((element) => RegExp(r'[a-zA-Z]').hasMatch(element))
      .map((e) => e.toLowerCase())
      .toList();

  String a = list.join('');
  String b = list.reversed.join('');

  return a == b ? 'true' : 'false';
}

void main() {
  print(PalindromeTwo('Anne, I vote more cars race Rome-to-Vienna'));
  print(PalindromeTwo('Noel - sees Leon'));
  print(PalindromeTwo('A war at Tarawa!'));
}
