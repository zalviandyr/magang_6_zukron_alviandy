String SudokuQuadrantChecker(List strArr) {
  List<int> quadrants = [];
  // to boards
  List<List<String>> boards = [];
  for (int i = 0; i < strArr.length; i++) {
    String temp = strArr[i];
    boards.add(temp.substring(1, 18).split(','));
  }

  // check each row
  Row:
  for (int i = 0; i < boards.length; i++) {
    for (int j = 0; j < boards[i].length; j++) {
      if (boards[i][j] != 'x') {
        for (int k = j + 1; k < boards[i].length; k++) {
          if (boards[i][j] == boards[i][k]) {
            // print(toQuadrant(i, j));
            // print(toQuadrant(i, k));
            quadrants.add(toQuadrant(i, j));
            quadrants.add(toQuadrant(i, k));
            break Row;
          }
        }
      }
    }
  }

  // check each col (up to bottom)
  for (int i = 0; i < boards.length; i++) {
    Col:
    for (int j = 0; j < boards[i].length; j++) {
      for (int k = j + 1; k < boards[i].length; k++) {
        if (boards[j][i] != 'x') {
          if (boards[j][i] == boards[k][i]) {
            // print(boards[j][i]);
            // print(toQuadrant(j, i));
            // print(toQuadrant(k, i));
            quadrants.add(toQuadrant(j, i));
            quadrants.add(toQuadrant(k, i));
            break Col;
          }
        }
      }
    }
  }

  // check each col (reverse - bottom to up)
  for (int i = boards.length - 1; i >= 0; i--) {
    Col:
    for (int j = boards.length - 1; j >= 0; j--) {
      for (int k = boards.length - 1; k >= j + 1; k--) {
        if (boards[j][i] != 'x') {
          if (boards[j][i] == boards[k][i]) {
            // print(boards[j][i]);
            // print(toQuadrant(j, i));
            // print(toQuadrant(k, i));
            quadrants.add(toQuadrant(j, i));
            quadrants.add(toQuadrant(k, i));
            break Col;
          }
        }
      }
    }
  }

  // check 3x3
  List<List<Map<String, Map<int, int>>>> matrix3 = [];
  for (int i = 0; i < boards.length; i += 3) {
    for (int j = 0; j < boards.length; j += 3) {
      List<Map<String, Map<int, int>>> temp = [];
      for (int k = i; k < i + 3; k++) {
        for (int l = j; l < j + 3; l++) {
          // print('$k $l ${boards[k][l]}');
          // temp.add(boards[k][l]);
          temp.add({
            boards[k][l]: {i: k, j: l}
          });

          // for (int m = 0; m < boards.length; m++) {}

          // stdout.write('${boards[k][l]} \t');
        }
      }
      matrix3.add(temp);
    }
    // print('\n');
  }

  Row:
  for (int i = 0; i < matrix3.length; i++) {
    for (int j = 0; j < matrix3[i].length; j++) {
      if (matrix3[i][j].keys.first != 'x') {
        for (int k = j + 1; k < matrix3[i].length; k++) {
          if (matrix3[i][j].keys.first == matrix3[i][k].keys.first) {
            // print(matrix3[i][j]);
            // print(matrix3[i][k]);
            // print(matrix3[i][j].values.first.values.first);
            // print(matrix3[i][j].values.first.values.last);
            quadrants.add(toQuadrant(matrix3[i][j].values.first.values.first,
                matrix3[i][j].values.first.values.last));

            break Row;
          }
        }
      }
    }
  }

  quadrants.sort();
  return quadrants.length > 0 ? quadrants.toSet().join(',') : 'legal';
}

int toQuadrant(int i, int j) {
  if (i >= 0 && i <= 2 && j >= 0 && j <= 2) return 1;
  if (i >= 0 && i <= 2 && j >= 3 && j <= 5) return 2;
  if (i >= 0 && i <= 2 && j >= 6 && j <= 8) return 3;
  if (i >= 3 && i <= 5 && j >= 0 && j <= 2) return 4;
  if (i >= 3 && i <= 5 && j >= 3 && j <= 5) return 5;
  if (i >= 3 && i <= 5 && j >= 6 && j <= 8) return 6;
  if (i >= 6 && i <= 8 && j >= 0 && j <= 2) return 7;
  if (i >= 6 && i <= 8 && j >= 3 && j <= 5) return 8;
  if (i >= 6 && i <= 8 && j >= 6 && j <= 8) return 9;

  return 0;
}

void main() {
  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,1)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(1,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)"
  ]));
  // 1,3,4

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(x,x,x,x,x,x,x,x,x)",
    "(6,x,5,x,3,x,x,4,x)",
    "(2,x,1,1,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,9)"
  ]));
  // 3,4,5,9

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(x,x,x,x,x,x,x,x,x)",
    "(6,x,5,x,3,x,x,4,x)",
    "(2,x,1,5,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,8)"
  ]));
  // legal

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(x,x,x,x,x,x,x,x,x)",
    "(6,x,5,x,3,x,x,4,x)",
    "(2,x,1,5,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,4)",
    "(9,1,2,3,4,5,6,7,8)"
  ]));
  // legal

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(x,x,x,x,x,x,x,x,x)",
    "(6,x,5,x,3,x,x,4,x)",
    "(2,x,1,5,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,9)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,9)",
    "(9,1,2,3,4,5,6,7,8)"
  ]));
  // 3,6,9

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(4,5,6,x,x,x,x,x,x)",
    "(7,8,9,x,x,x,x,x,x)",
    "(2,3,4,x,x,x,x,x,x)",
    "(5,6,7,x,x,x,x,x,x)",
    "(8,9,1,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,1)"
  ]));
  // legal

  print(SudokuQuadrantChecker([
    "(1,2,3,4,5,6,7,8,9)",
    "(4,5,6,1,2,3,x,x,x)",
    "(7,8,9,x,x,6,x,x,x)",
    "(2,3,4,x,x,x,x,x,x)",
    "(5,6,7,x,x,x,x,x,x)",
    "(8,9,1,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,x)",
    "(x,x,x,x,x,x,x,x,1)"
  ]));
  // 2
}
