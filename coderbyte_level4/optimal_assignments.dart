String OptimalAssignments(List strArr) {
  List<List<int>> matrix = [];
  for (var item in strArr) {
    String str = item;
    List<int> temp = str
        .substring(1, str.length - 1)
        .split(',')
        .map((e) => int.parse(e))
        .toList();

    matrix.add(temp);
  }

  List<int> arr = List.generate(matrix.length, (index) => index);
  List<List<int>> permutations = [];
  permute(arr, permutations, 0);

  int? minCost = null;
  List<int> minCostCombination = [];

  permutations.forEach((element) {
    int cost = 0;
    for (int i = 0; i < element.length; i++) {
      cost += matrix[i][element[i]];
    }
    if (minCost == null || (minCost != null && cost < minCost!)) {
      minCost = cost;
      minCostCombination = element;
    }
  });

  String result = '';
  for (int i = 0; i < minCostCombination.length; i++) {
    result += '(${i + 1}-${minCostCombination[i] + 1})';
  }

  return result;
}

void permute(List<int> array, List<List<int>> permutations, int start) {
  if (start == array.length) {
    List<int> temp = [];
    for (int i = 0; i < array.length; i++) {
      temp.add(array[i]);
    }
    permutations.add(temp);
    return;
  }

  for (int i = start; i < array.length; i++) {
    int temp = array[i];
    array[i] = array[start];
    array[start] = temp;

    permute(array, permutations, start + 1);

    temp = array[i];
    array[i] = array[start];
    array[start] = temp;
  }
}

void main() {
  print(OptimalAssignments(["(1,2,1)", "(4,1,5)", "(5,2,1)"]));
  print(OptimalAssignments(["(5,4,2)", "(12,4,3)", "(3,4,13)"]));
  print(OptimalAssignments(
      ["(13,4,7,6)", "(1,11,5,4)", "(6,7,2,8)", "(1,3,5,9)"]));
  print(OptimalAssignments(
      ["(2500,4000,3500)", "(4000,6000,3500)", "(2000,4000,2500)"]));
}
