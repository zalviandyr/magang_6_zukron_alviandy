import 'package:book_rent_flutter/models/models.dart';
import 'package:equatable/equatable.dart';

abstract class GenreState extends Equatable {
  GenreState();
}

class GenreUninitialized extends GenreState {
  @override
  List<Object?> get props => [];
}

class GenreLoading extends GenreState {
  @override
  List<Object?> get props => [];
}

class GenreError extends GenreState {
  @override
  List<Object?> get props => [];
}

class GenreFetchSuccess extends GenreState {
  final List<Genre> genres;

  GenreFetchSuccess({required this.genres});

  @override
  List<Object?> get props => [genres];
}
