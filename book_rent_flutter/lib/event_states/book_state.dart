import 'package:book_rent_flutter/models/models.dart';
import 'package:equatable/equatable.dart';

abstract class BookState extends Equatable {
  BookState();
}

class BookUninitialized extends BookState {
  @override
  List<Object?> get props => [];
}

class BookLoading extends BookState {
  @override
  List<Object?> get props => [];
}

class BookError extends BookState {
  @override
  List<Object?> get props => [];
}

class BookFetchSuccess extends BookState {
  final List<Book> books;

  BookFetchSuccess({required this.books});

  @override
  List<Object?> get props => [books];
}

class BookPublishFetchSuccess extends BookState {
  final List<Book> books;

  BookPublishFetchSuccess({required this.books});

  @override
  List<Object?> get props => [books];
}

class BookRentFetchSuccess extends BookState {
  final List<Book> books;

  BookRentFetchSuccess({required this.books});

  @override
  List<Object?> get props => [books];
}

class BookCreateSuccess extends BookState {
  @override
  List<Object?> get props => [];
}

class BookCreateFailed extends BookState {
  @override
  List<Object?> get props => [];
}

class BookUpdateSuccess extends BookState {
  @override
  List<Object?> get props => [];
}

class BookUpdateFailed extends BookState {
  @override
  List<Object?> get props => [];
}

class BookDeleteSuccess extends BookState {
  @override
  List<Object?> get props => [];
}

class BookDeleteFailed extends BookState {
  @override
  List<Object?> get props => [];
}
