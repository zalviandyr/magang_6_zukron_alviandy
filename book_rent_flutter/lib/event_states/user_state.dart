import 'package:book_rent_flutter/models/models.dart';
import 'package:equatable/equatable.dart';

abstract class UserState extends Equatable {
  UserState();
}

class UserUninitialized extends UserState {
  @override
  List<Object?> get props => [];
}

class UserLoading extends UserState {
  @override
  List<Object?> get props => [];
}

class UserError extends UserState {
  @override
  List<Object?> get props => [];
}

class UserRegisterInProcess extends UserState {
  final User user;
  UserRegisterInProcess({required this.user});

  @override
  List<Object?> get props => [];
}

class UserRegisterSuccess extends UserState {
  @override
  List<Object?> get props => [];
}

class UserRegisterFailed extends UserState {
  @override
  List<Object?> get props => [];
}

class UserLoginSuccess extends UserState {
  final User user;

  UserLoginSuccess({required this.user});
  @override
  List<Object?> get props => [];
}

class UserLoginFailed extends UserState {
  @override
  List<Object?> get props => [];
}
