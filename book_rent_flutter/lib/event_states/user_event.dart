import 'package:book_rent_flutter/models/models.dart';
import 'package:equatable/equatable.dart';

abstract class UserEvent extends Equatable {
  UserEvent();
}

class UserRegisterProcessed extends UserEvent {
  final User user;
  UserRegisterProcessed({required this.user});

  @override
  List<Object?> get props => [];
}

class UserRegister extends UserEvent {
  final User user;
  UserRegister({required this.user});

  @override
  List<Object?> get props => [user];
}

class UserLogin extends UserEvent {
  final String email;
  final String password;
  UserLogin({required this.email, required this.password});

  @override
  List<Object?> get props => [email, password];
}
