import 'package:book_rent_flutter/models/book.dart';
import 'package:equatable/equatable.dart';

abstract class BookEvent extends Equatable {
  BookEvent();
}

class BookFetch extends BookEvent {
  @override
  List<Object?> get props => [];
}

class BookPublishFetch extends BookEvent {
  @override
  List<Object?> get props => [];
}

class BookRentFetch extends BookEvent {
  @override
  List<Object?> get props => [];
}

class BookRent extends BookEvent {
  final Book book;

  BookRent({required this.book});

  @override
  List<Object?> get props => [book];
}

class BookReturn extends BookEvent {
  final Book book;

  BookReturn({required this.book});

  @override
  List<Object?> get props => [book];
}

class BookCreate extends BookEvent {
  final Book book;

  BookCreate({required this.book});

  @override
  List<Object?> get props => [book];
}

class BookUpdate extends BookEvent {
  final Book book;

  BookUpdate({required this.book});

  @override
  List<Object?> get props => [book];
}

class BookDelete extends BookEvent {
  final Book book;

  BookDelete({required this.book});

  @override
  List<Object?> get props => [book];
}
