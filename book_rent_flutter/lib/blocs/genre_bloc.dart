import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/services/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenreBloc extends Bloc<GenreEvent, GenreState> {
  GenreBloc() : super(GenreUninitialized());

  @override
  Stream<GenreState> mapEventToState(GenreEvent event) async* {
    try {
      if (event is GenreFetch) {
        yield GenreLoading();

        List<Genre> genres = await GenreService.getGenre(await _getLoginUser);

        yield GenreFetchSuccess(genres: genres);
      }
    } catch (e) {
      print(e);

      yield GenreError();
    }
  }

  Future<User> get _getLoginUser async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return User(
      email: sharedPreferences.getString('email'),
      password: sharedPreferences.getString('password'),
    );
  }
}
