import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/services/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserUninitialized());

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    try {
      if (event is UserRegisterProcessed) {
        yield UserLoading();

        yield UserRegisterInProcess(user: event.user);
      }

      if (event is UserRegister) {
        yield UserLoading();

        bool isRegisterSuccess = await UserService.register(event.user);
        if (isRegisterSuccess) {
          yield UserRegisterSuccess();
        } else {
          yield UserRegisterFailed();
        }
      }

      if (event is UserLogin) {
        yield UserLoading();

        User? user = await UserService.login(event.email, event.password);

        if (user != null) {
          // save data user
          SharedPreferences preferences = await SharedPreferences.getInstance();
          preferences.setString('email', event.email);
          preferences.setString('password', event.password);
          preferences.setBool('isPeminjam', user.isPeminjam);

          yield UserLoginSuccess(user: user);
        } else {
          yield UserLoginFailed();
        }
      }
    } catch (e) {
      print(e);

      yield UserError();
    }
  }

  Future<User?> userLogin() async {
    User? user;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? email = preferences.getString('email');
    String? password = preferences.getString('password');
    bool? isPeminjam = preferences.getBool('isPeminjam');
    if (email != null && password != null) {
      user = User(
        email: email,
        password: password,
        isPeminjam: isPeminjam ?? false,
      );
    }

    return user;
  }

  Future<bool> userLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.clear();
  }
}
