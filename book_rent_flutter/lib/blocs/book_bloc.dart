import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/services/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookBloc extends Bloc<BookEvent, BookState> {
  BookBloc() : super(BookUninitialized());

  @override
  Stream<BookState> mapEventToState(BookEvent event) async* {
    try {
      if (event is BookFetch) {
        yield BookLoading();

        List<Book> books = await PemilikBookService.getAll(await _getLoginUser);

        yield BookFetchSuccess(books: books);
      }

      if (event is BookPublishFetch) {
        yield BookLoading();

        List<Book> books =
            await PeminjamBookService.getAll(await _getLoginUser);

        yield BookPublishFetchSuccess(books: books);
      }

      if (event is BookRentFetch) {
        yield BookLoading();

        List<Book> books =
            await PeminjamBookService.getAllBookRent(await _getLoginUser);

        yield BookRentFetchSuccess(books: books);
      }

      if (event is BookRent) {
        yield BookLoading();

        await PeminjamBookService.rent(await _getLoginUser, event.book);

        List<Book> books =
            await PeminjamBookService.getAll(await _getLoginUser);

        yield BookPublishFetchSuccess(books: books);
      }

      if (event is BookReturn) {
        yield BookLoading();

        await PeminjamBookService.returnBook(await _getLoginUser, event.book);

        List<Book> books =
            await PeminjamBookService.getAllBookRent(await _getLoginUser);

        yield BookRentFetchSuccess(books: books);
      }

      if (event is BookCreate) {
        yield BookLoading();

        bool isSuccess =
            await PemilikBookService.create(await _getLoginUser, event.book);

        if (isSuccess) {
          yield BookCreateSuccess();
        } else {
          yield BookCreateFailed();
        }
      }

      if (event is BookUpdate) {
        yield BookLoading();

        bool isSuccess =
            await PemilikBookService.update(await _getLoginUser, event.book);

        if (isSuccess) {
          yield BookUpdateSuccess();
        } else {
          yield BookUpdateFailed();
        }
      }

      if (event is BookDelete) {
        yield BookLoading();

        bool isSuccess =
            await PemilikBookService.delete(await _getLoginUser, event.book);

        if (isSuccess) {
          yield BookDeleteSuccess();
        } else {
          yield BookDeleteFailed();
        }
      }
    } catch (e) {
      print(e);

      yield BookError();
    }
  }

  Future<User> get _getLoginUser async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return User(
      email: sharedPreferences.getString('email'),
      password: sharedPreferences.getString('password'),
    );
  }
}
