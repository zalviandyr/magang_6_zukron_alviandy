import 'package:equatable/equatable.dart';

class Genre extends Equatable {
  final int id;
  final String genre;

  Genre({required this.id, required this.genre});

  factory Genre.fromJson(dynamic json) {
    return Genre(
      id: json['id'],
      genre: json['genre'],
    );
  }

  static List<Genre> fromJsonToList(dynamic json) {
    List<Genre> genres = [];

    for (var data in json) {
      genres.add(Genre(
        id: data['id'],
        genre: data['genre'],
      ));
    }

    return genres;
  }

  @override
  List<Object?> get props => [id, genre];
}
