import 'package:book_rent_flutter/models/models.dart';

class Book {
  int? id;
  Genre? genre;
  String? title;
  int? price;
  String? priceDesc;
  String? image;
  User? pemilik;
  User? peminjam;
  bool? isPublish;
  String? status;

  Book({
    this.id,
    this.genre,
    this.title,
    this.price,
    this.priceDesc,
    this.image,
    this.pemilik,
    this.peminjam,
    this.isPublish,
    this.status,
  });

  factory Book.fromJson(dynamic json) {
    return Book(
      id: json['id'],
      title: json['title'],
      image: json['image'],
      genre: Genre.fromJson(json['genre']),
      price: json['price'],
      priceDesc: json['price_desc'],
      status: json['status'],
      isPublish: json['is_publish'] == 1,
      pemilik: User.fromJson(json['pemilik']),
      peminjam:
          json['peminjam'] != null ? User.fromJson(json['peminjam']) : null,
    );
  }

  static List<Book> fromJsonToList(dynamic json) {
    List<Book> books = [];

    for (var data in json) {
      books.add(
        Book(
          id: data['id'],
          title: data['title'],
          image: data['image'],
          genre: Genre.fromJson(data['genre']),
          price: data['price'],
          priceDesc: data['price_desc'],
          status: data['status'],
          isPublish: data['is_publish'] == 1,
          pemilik: User.fromJson(data['pemilik']),
          peminjam:
              data['peminjam'] != null ? User.fromJson(data['peminjam']) : null,
        ),
      );
    }

    return books;
  }
}
