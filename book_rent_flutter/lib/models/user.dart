class User {
  String? name;
  String? email;
  String? password;
  String? phoneNumber;
  String? address;
  String? deviceId;
  bool isPeminjam;

  User({
    this.name,
    this.email,
    this.password,
    this.phoneNumber,
    this.address,
    this.deviceId,
    this.isPeminjam = true,
  });

  factory User.fromJson(dynamic json) {
    return User(
      name: json['name'],
      email: json['email'],
      password: null,
      phoneNumber: json['phone_number'],
      address: json['address'],
      deviceId: json['device_id'],
      isPeminjam: json['role_id'] == 2,
    );
  }
}
