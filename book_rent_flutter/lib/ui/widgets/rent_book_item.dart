import 'package:book_rent_flutter/models/models.dart';
import 'package:flutter/material.dart';

class RentBookItem extends StatelessWidget {
  final Book book;
  final Function(Book) onDescriptionPressed;
  final Function(Book) onReturnPressed;

  const RentBookItem({
    Key? key,
    required this.book,
    required this.onDescriptionPressed,
    required this.onReturnPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 5.0,
            offset: Offset(1, 2),
          ),
        ],
      ),
      child: Row(
        children: [
          Image(
            image: NetworkImage(book.image!),
            width: 100.0,
            height: 100.0,
            fit: BoxFit.cover,
          ),
          const SizedBox(width: 20.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  book.title!,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 5.0),
                Text(
                  book.priceDesc!,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 15.0),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () => onDescriptionPressed(book),
                        child: Text('Deskripsi'),
                      ),
                    ),
                    const SizedBox(width: 20.0),
                    Expanded(
                      child: TextButton(
                        onPressed: () => onReturnPressed(book),
                        child: Text('Kembalikan Buku'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
