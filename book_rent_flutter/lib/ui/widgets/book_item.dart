import 'package:flutter/material.dart';

class BookItem extends StatelessWidget {
  final String image;
  final String title;
  final bool isPublish;
  final VoidCallback onEditPressed;
  final VoidCallback onDeletePressed;

  const BookItem({
    Key? key,
    required this.image,
    required this.title,
    required this.isPublish,
    required this.onEditPressed,
    required this.onDeletePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 5.0,
            offset: Offset(1, 2),
          ),
        ],
      ),
      child: Row(
        children: [
          Image(
            image: NetworkImage(image),
            width: 100.0,
            height: 100.0,
            fit: BoxFit.cover,
          ),
          const SizedBox(width: 20.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 5.0),
                Text(
                  isPublish
                      ? 'Buku dipublikasikan ke public'
                      : 'Belum dipublikasi',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 15.0),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: onEditPressed,
                        child: Text('Edit'),
                      ),
                    ),
                    const SizedBox(width: 20.0),
                    Expanded(
                      child: TextButton(
                        onPressed: onDeletePressed,
                        child: Text(
                          'Hapus',
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
