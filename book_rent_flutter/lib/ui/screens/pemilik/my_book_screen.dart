import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/constants/enum.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:book_rent_flutter/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyBookScreen extends StatefulWidget {
  const MyBookScreen({Key? key}) : super(key: key);

  @override
  _MyBookScreenState createState() => _MyBookScreenState();
}

class _MyBookScreenState extends State<MyBookScreen> {
  late BookBloc _bookBloc;

  @override
  void initState() {
    // init bloc
    _bookBloc = BlocProvider.of<BookBloc>(context);

    super.initState();
  }

  void _floatingAction() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => AddEditBookScreen(section: AddEditBookSection.add),
      ),
    );
  }

  void _bookItemEditAction(Book book) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => AddEditBookScreen(
          section: AddEditBookSection.edit,
          book: book,
        ),
      ),
    );
  }

  void _bookItemDeleteAction(Book book) {
    _bookBloc.add(BookDelete(book: book));
  }

  void _myBookListener(BuildContext context, BookState state) {
    if (state is BookDeleteSuccess) {
      showSnackbar(context, 'Berhasil menghapus buku');

      // refetch data
      _bookBloc.add(BookFetch());
    } else if (state is BookDeleteFailed) {
      showSnackbar(context, 'Gagal manghapus buku', isError: true);
    } else if (state is BookError) {
      showSnackbar(context, 'Ups... ada yang salah nih', isError: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<BookBloc, BookState>(
        listener: _myBookListener,
        builder: (context, state) {
          if (state is BookFetchSuccess) {
            return ListView.builder(
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                Book book = state.books[index];

                return BookItem(
                  image: book.image!,
                  title: book.title!,
                  isPublish: book.isPublish!,
                  onEditPressed: () => _bookItemEditAction(book),
                  onDeletePressed: () => _bookItemDeleteAction(book),
                );
              },
              itemCount: state.books.length,
            );
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _floatingAction,
        child: Icon(Icons.add, color: Colors.white),
      ),
    );
  }
}
