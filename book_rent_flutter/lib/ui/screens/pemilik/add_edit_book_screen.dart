import 'dart:io';

import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/constants/enum.dart';
import 'package:book_rent_flutter/ui/configs/pallette.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker_gallery_camera/image_picker_gallery_camera.dart';

class AddEditBookScreen extends StatefulWidget {
  final AddEditBookSection section;
  final Book? book;

  const AddEditBookScreen({
    Key? key,
    required this.section,
    this.book,
  }) : super(key: key);

  @override
  _AddEditBookScreenState createState() => _AddEditBookScreenState();
}

class _AddEditBookScreenState extends State<AddEditBookScreen> {
  late GenreBloc _genreBloc;
  late BookBloc _bookBloc;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _titleController;
  late TextEditingController _priceController;
  late String? _imagePath;
  late Genre? _genreSelected;
  late bool _isPublish;

  @override
  void initState() {
    // init bloc
    _genreBloc = BlocProvider.of<GenreBloc>(context);
    _bookBloc = BlocProvider.of<BookBloc>(context);

    // init form
    _formKey = GlobalKey();
    _titleController = TextEditingController();
    _priceController = TextEditingController();

    // set data when is edit
    if (widget.section == AddEditBookSection.edit) {
      _imagePath = widget.book!.image;
      _genreSelected = widget.book!.genre;
      _isPublish = widget.book!.isPublish!;

      _titleController.text = widget.book!.title!;
      _priceController.text = widget.book!.price!.toString();
    }

    // set
    if (widget.section == AddEditBookSection.add) {
      _imagePath = null;
      _genreSelected = null;
      _isPublish = false;
    }

    // fetch data
    _genreBloc.add(GenreFetch());

    super.initState();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _priceController.dispose();

    super.dispose();
  }

  ImageProvider? get _bookImage {
    if (_imagePath != null && File(_imagePath!).existsSync()) {
      return FileImage(File(_imagePath!));
    }

    if (_imagePath != null && _imagePath!.contains('http')) {
      return NetworkImage(_imagePath!);
    }

    return AssetImage('images/book.png');
  }

  String? _formValidator(String? value) {
    if (value != null && value.isEmpty) {
      return 'Wajib diisi';
    }

    return null;
  }

  void _addImageAction() async {
    dynamic image = await ImagePickerGC.pickImage(
        context: context, source: ImgSource.Gallery);

    if (image != null) {
      setState(() => _imagePath = image.path);
    }
  }

  void _genreDropdownAction(Genre? genre) {
    setState(() => _genreSelected = genre);
  }

  void _publishCheckboxAction(bool? value) {
    setState(() => _isPublish = value ?? false);
  }

  void _addUpdateAction() {
    if (_formKey.currentState!.validate()) {
      String title = _titleController.text.trim();
      int price = int.parse(_priceController.text.trim());

      if (widget.section == AddEditBookSection.add) {
        Book book = Book(
          genre: _genreSelected,
          title: title,
          price: price,
          image: _imagePath,
          isPublish: _isPublish,
        );

        _bookBloc.add(BookCreate(book: book));
      }

      if (widget.section == AddEditBookSection.edit) {
        Book book = Book(
          id: widget.book!.id,
          genre: _genreSelected,
          title: title,
          price: price,
          isPublish: _isPublish,
        );

        _bookBloc.add(BookUpdate(book: book));
      }
    }
  }

  void _bookListener(BuildContext context, BookState state) {
    if (state is BookCreateSuccess) {
      showSnackbar(context, 'Berhasil menambahkan buku');

      // re fetch data buku
      _bookBloc.add(BookFetch());
      Navigator.pop(context);
    } else if (state is BookCreateFailed) {
      showSnackbar(context, 'Gagal menambahkan buku', isError: true);
    } else if (state is BookUpdateSuccess) {
      showSnackbar(context, 'Berhasil mengubah buku');

      // re fetch data buku
      _bookBloc.add(BookFetch());
      Navigator.pop(context);
    } else if (state is BookUpdateFailed) {
      showSnackbar(context, 'Gagal mengubah buku', isError: true);
    } else if (state is BookError) {
      showSnackbar(context, 'Ups... ada yang salah nih', isError: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: (widget.section == AddEditBookSection.add)
              ? Text('Add Book')
              : Text('Edit Book')),
      body: BlocListener<BookBloc, BookState>(
        listener: _bookListener,
        child: BlocBuilder<GenreBloc, GenreState>(
          builder: (context, genreState) {
            if (genreState is GenreFetchSuccess) {
              return Form(
                key: _formKey,
                child: ListView(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  children: [
                    Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        Stack(
                          children: [
                            Container(
                              margin: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(0, 1),
                                  ),
                                ],
                              ),
                              child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: _bookImage,
                                radius: 70.0,
                              ),
                            ),
                            (widget.section == AddEditBookSection.add)
                                ? Positioned(
                                    bottom: 5,
                                    right: -15,
                                    child: MaterialButton(
                                      onPressed: _addImageAction,
                                      color: Theme.of(context).accentColor,
                                      shape: CircleBorder(),
                                      padding: const EdgeInsets.all(10.0),
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    TextFormField(
                      controller: _titleController,
                      validator: _formValidator,
                      decoration:
                          Pallette.inputDecoration.copyWith(labelText: 'Judul'),
                    ),
                    const SizedBox(height: 10.0),
                    FormField(builder: (state) {
                      return InputDecorator(
                        decoration: Pallette.inputDecoration.copyWith(
                          labelText: 'Genre',
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: 15.0, vertical: 5.0),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<Genre>(
                              isExpanded: true,
                              hint: Text('Pilih Genre'),
                              onChanged: _genreDropdownAction,
                              value: _genreSelected,
                              items: genreState.genres.map((e) {
                                return DropdownMenuItem(
                                  value: e,
                                  child: Text(e.genre),
                                );
                              }).toList()),
                        ),
                      );
                    }),
                    const SizedBox(height: 10.0),
                    Row(
                      children: [
                        Text(
                          'Rp.',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        const SizedBox(width: 15.0),
                        Expanded(
                          child: TextFormField(
                            controller: _priceController,
                            validator: _formValidator,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'[0-9]'))
                            ],
                            decoration: Pallette.inputDecoration.copyWith(
                              labelText: 'Harga',
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Checkbox(
                          value: _isPublish,
                          onChanged: _publishCheckboxAction,
                        ),
                        Text(
                          'Apakah buku ini di publish ? ',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    const SizedBox(height: 20.0),
                    Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        BlocBuilder<BookBloc, BookState>(
                          builder: (context, state) {
                            if (state is BookLoading) {
                              return CircularProgressIndicator();
                            }

                            return ElevatedButton(
                              onPressed: _addUpdateAction,
                              child: (widget.section == AddEditBookSection.add)
                                  ? Text('Tambah')
                                  : Text('Ubah'),
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }

            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
