import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:book_rent_flutter/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyRentScreen extends StatefulWidget {
  @override
  _MyRentScreenState createState() => _MyRentScreenState();
}

class _MyRentScreenState extends State<MyRentScreen> {
  late BookBloc _bookBloc;

  @override
  void initState() {
    // bloc
    _bookBloc = BlocProvider.of<BookBloc>(context);

    super.initState();
  }

  void _descriptionBookAction(Book book) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => DescriptionBook(book: book)),
    );
  }

  void _returnBookAction(Book book) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Kembalikan buku'),
            content: Text('Yakin ingin mengembalikan buku ${book.title}'),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    _bookBloc.add(BookReturn(book: book));

                    showSnackbar(context, 'Berhasil mengembalikan buku');

                    Navigator.pop(context);
                  },
                  child: Text('Iya')),
              const SizedBox(
                width: 10.0,
              ),
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Tidak',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<BookBloc, BookState>(
        builder: (context, state) {
          if (state is BookRentFetchSuccess) {
            return ListView.builder(
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                Book book = state.books[index];

                return RentBookItem(
                  book: book,
                  onDescriptionPressed: _descriptionBookAction,
                  onReturnPressed: _returnBookAction,
                );
              },
              itemCount: state.books.length,
            );
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
