import 'package:book_rent_flutter/ui/widgets/snackbar.dart';
import 'package:flutter/material.dart';

class GestureScreen extends StatefulWidget {
  @override
  _GestureScreenState createState() => _GestureScreenState();
}

class _GestureScreenState extends State<GestureScreen> {
  late double _containerDx;

  @override
  void initState() {
    // set
    _containerDx = 0;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(10.0),
            height: 100.0,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey,
            child: Stack(
              children: [
                Positioned(
                  left: _containerDx,
                  child: GestureDetector(
                    onHorizontalDragUpdate: (details) {
                      setState(() {
                        double screenWidth =
                            MediaQuery.of(context).size.width - 120.0;
                        double position = details.globalPosition.dx - 50.0;

                        if (position > 0 && position < screenWidth) {
                          _containerDx = position;
                        }
                      });
                    },
                    child: Container(
                      width: 100.0,
                      height: 80.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 50.0),
          GestureDetector(
            onLongPress: () {
              showSnackbar(context, 'Long Pressed');
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              color: Colors.blue.shade400,
              alignment: Alignment.center,
              child: Text('Long Pressed'),
            ),
          ),
          const SizedBox(height: 20.0),
          GestureDetector(
            onDoubleTap: () {
              showSnackbar(context, 'Double Tap');
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              color: Colors.green.shade400,
              alignment: Alignment.center,
              child: Text('Double Tap'),
            ),
          ),
        ],
      ),
    );
  }
}
