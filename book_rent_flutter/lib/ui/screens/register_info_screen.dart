import 'package:book_rent_flutter/blocs/user_bloc.dart';
import 'package:book_rent_flutter/ui/configs/pallette.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:book_rent_flutter/ui/widgets/snackbar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterInfoScreen extends StatefulWidget {
  @override
  _RegisterInfoScreenState createState() => _RegisterInfoScreenState();
}

class _RegisterInfoScreenState extends State<RegisterInfoScreen> {
  late UserBloc _userBloc;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _addressController;
  late TextEditingController _phoneNumberController;
  late User _user;

  @override
  void initState() {
    // init bloc
    _userBloc = BlocProvider.of<UserBloc>(context);

    // init controller
    _addressController = TextEditingController();
    _phoneNumberController = TextEditingController();

    // init key
    _formKey = GlobalKey();

    // init
    UserState state = _userBloc.state;
    if (state is UserRegisterInProcess) {
      _user = state.user;
    }

    super.initState();
  }

  @override
  void dispose() {
    _addressController.dispose();
    _phoneNumberController.dispose();

    super.dispose();
  }

  String? _formValidator(String? value) {
    if (value != null && value.isEmpty) return 'Wajib diisi';

    return null;
  }

  void _toLoginScreen() async {
    if (_formKey.currentState!.validate()) {
      String address = _addressController.text.trim();
      String phoneNumber = '+628' + _phoneNumberController.text.trim();

      _user.address = address;
      _user.phoneNumber = phoneNumber;
      _user.deviceId = await FirebaseMessaging.instance.getToken();
      _userBloc.add(UserRegister(user: _user));
    }
  }

  void _registerListener(BuildContext context, UserState state) {
    if (state is UserRegisterSuccess) {
      showSnackbar(context, 'Berhasil registrasi');

      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => LoginScreen()),
        (route) => false,
      );
    }

    if (state is UserRegisterFailed || state is UserError) {
      showSnackbar(context, 'Gagal registrasi', isError: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocListener<UserBloc, UserState>(
          listener: _registerListener,
          child: Form(
            key: _formKey,
            child: ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(20.0),
              children: [
                Text(
                  'Registrasi Rental Buku',
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(height: 35.0),
                TextFormField(
                  controller: _addressController,
                  validator: _formValidator,
                  maxLines: 3,
                  decoration: Pallette.inputDecoration.copyWith(
                    labelText: 'Alamat',
                    alignLabelWithHint: true,
                  ),
                ),
                const SizedBox(height: 15.0),
                Row(
                  children: [
                    Text(
                      '+62',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    const SizedBox(width: 15.0),
                    Expanded(
                      child: TextFormField(
                        controller: _phoneNumberController,
                        validator: _formValidator,
                        decoration: Pallette.inputDecoration.copyWith(
                          labelText: 'No. Telp',
                          hintText: '8...',
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 15.0),
                Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) {
                        if (state is UserLoading) {
                          return CircularProgressIndicator();
                        }

                        return MaterialButton(
                          color: Theme.of(context).accentColor,
                          textColor: Colors.white,
                          onPressed: _toLoginScreen,
                          child: Text('Daftar'),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
