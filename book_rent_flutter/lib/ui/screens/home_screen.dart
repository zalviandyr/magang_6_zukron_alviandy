import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:book_rent_flutter/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late BookBloc _bookBloc;

  @override
  void initState() {
    // init bloc
    _bookBloc = BlocProvider.of<BookBloc>(context);

    super.initState();
  }

  void _descriptionBookAction(Book book) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => DescriptionBook(book: book)),
    );
  }

  void _rentBookAction(Book book) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Pinjam buku'),
            content: Text('Yakin ingin meminjam buku ${book.title}'),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    _bookBloc.add(BookRent(book: book));

                    showSnackbar(context, 'Berhasil meminjam buku');

                    Navigator.pop(context);
                  },
                  child: Text('Iya')),
              const SizedBox(
                width: 10.0,
              ),
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Tidak',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ],
          );
        });
  }

  void _homeListener(BuildContext context, BookState state) {
    if (state is BookError) {
      showSnackbar(context, 'Ups... ada yang salah nih', isError: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<BookBloc, BookState>(
        listener: _homeListener,
        builder: (context, state) {
          if (state is BookPublishFetchSuccess) {
            return ListView.builder(
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                Book book = state.books[index];

                return PublishBookItem(
                  book: book,
                  onDescriptionPressed: _descriptionBookAction,
                  onRentPressed: _rentBookAction,
                );
              },
              itemCount: state.books.length,
            );
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
