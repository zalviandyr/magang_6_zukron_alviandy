import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/ui/configs/pallette.dart';
import 'package:book_rent_flutter/event_states/user_event.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late UserBloc _userBloc;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _confirmPasswordController;
  late bool _isCheckboxSelected;
  late String? _errorText;

  @override
  void initState() {
    // init bloc
    _userBloc = BlocProvider.of<UserBloc>(context);

    // key
    _formKey = GlobalKey();

    // init
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();

    // set
    _isCheckboxSelected = false;
    _errorText = null;

    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();

    super.dispose();
  }

  void _toRegisterInfoScreen() {
    if (_formKey.currentState!.validate()) {
      String name = _nameController.text.trim();
      String email = _emailController.text.trim();
      String password = _passwordController.text.trim();
      String confirmPassword = _confirmPasswordController.text.trim();
      bool isPeminjam = _isCheckboxSelected;

      if (password == confirmPassword) {
        User user = User(
          name: name,
          email: email,
          password: password,
          isPeminjam: isPeminjam,
        );

        _userBloc.add(UserRegisterProcessed(user: user));

        Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => RegisterInfoScreen(),
        ));
      } else {
        setState(() => _errorText = 'Password harus sama');
      }
    }
  }

  String? _formValidator(String? value) {
    if (value != null && value.isEmpty) return 'Wajib diisi';

    return null;
  }

  void _toLoginScreen() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            padding: const EdgeInsets.all(20.0),
            children: [
              Text(
                'Registrasi Rental Buku',
                style: Theme.of(context).textTheme.headline6,
              ),
              const SizedBox(height: 35.0),
              TextFormField(
                controller: _nameController,
                validator: _formValidator,
                decoration:
                    Pallette.inputDecoration.copyWith(labelText: 'Nama'),
              ),
              const SizedBox(height: 15.0),
              TextFormField(
                controller: _emailController,
                validator: _formValidator,
                decoration:
                    Pallette.inputDecoration.copyWith(labelText: 'Email'),
              ),
              const SizedBox(height: 15.0),
              TextFormField(
                controller: _passwordController,
                validator: _formValidator,
                obscureText: true,
                decoration: Pallette.inputDecoration.copyWith(
                  labelText: 'Password',
                  errorText: _errorText,
                ),
              ),
              const SizedBox(height: 15.0),
              TextFormField(
                controller: _confirmPasswordController,
                validator: _formValidator,
                obscureText: true,
                decoration: Pallette.inputDecoration.copyWith(
                  labelText: 'Confirm Password',
                  errorText: _errorText,
                ),
              ),
              const SizedBox(height: 15.0),
              Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Checkbox(
                      value: _isCheckboxSelected,
                      onChanged: (isSelected) =>
                          setState(() => _isCheckboxSelected = isSelected!),
                    ),
                    Text(
                      'Apakah anda sebagai peminjam ? ',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 15.0),
              Wrap(
                alignment: WrapAlignment.center,
                children: [
                  MaterialButton(
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                    onPressed: _toRegisterInfoScreen,
                    child: Text('Daftar'),
                  ),
                ],
              ),
              const SizedBox(height: 15.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Sudah punya akun ? '),
                  TextButton(
                    onPressed: _toLoginScreen,
                    child: Text('masuk'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
