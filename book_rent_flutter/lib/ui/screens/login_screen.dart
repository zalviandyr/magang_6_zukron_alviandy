import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/constants/enum.dart';
import 'package:book_rent_flutter/ui/configs/pallette.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:book_rent_flutter/ui/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late UserBloc _userBloc;
  late GlobalKey<FormState> _formKey;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    // init bloc
    _userBloc = BlocProvider.of<UserBloc>(context);

    // init
    _formKey = GlobalKey();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    // check user
    _userBloc.userLogin().then((value) {
      if (value != null) {
        _userBloc
            .add(UserLogin(email: value.email!, password: value.password!));
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();

    super.dispose();
  }

  void _toRegisterScreen() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (_) => RegisterScreen(),
    ));
  }

  String? _formValidator(String? value) {
    if (value != null && value.isEmpty) {
      return 'Wajib diisi';
    }

    return null;
  }

  void _loginAction() {
    if (_formKey.currentState!.validate()) {
      String email = _emailController.text.trim();
      String password = _passwordController.text.trim();

      _userBloc.add(UserLogin(email: email, password: password));
    }
  }

  void _loginListener(BuildContext context, UserState state) {
    if (state is UserLoginSuccess) {
      showSnackbar(context, 'Berhasil login');

      if (state.user.isPeminjam) {
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (_) => DrawerScreen(section: DrawerSection.home)),
          (route) => false,
        );
      } else if (!state.user.isPeminjam) {
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (_) => DrawerScreen(section: DrawerSection.myBook)),
          (route) => false,
        );
      }
    }

    if (state is UserLoginFailed || state is UserError) {
      showSnackbar(context, 'Gagal login', isError: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocListener<UserBloc, UserState>(
          listener: _loginListener,
          child: Form(
            key: _formKey,
            child: ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(20.0),
              children: [
                Text(
                  'Login Rental Buku',
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(height: 35.0),
                TextFormField(
                  controller: _emailController,
                  validator: _formValidator,
                  decoration:
                      Pallette.inputDecoration.copyWith(labelText: 'Email'),
                ),
                const SizedBox(height: 15.0),
                TextFormField(
                  controller: _passwordController,
                  validator: _formValidator,
                  obscureText: true,
                  decoration:
                      Pallette.inputDecoration.copyWith(labelText: 'Password'),
                ),
                const SizedBox(height: 15.0),
                Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) {
                        if (state is UserLoading) {
                          return CircularProgressIndicator();
                        }

                        return MaterialButton(
                          color: Theme.of(context).accentColor,
                          textColor: Colors.white,
                          onPressed: _loginAction,
                          child: Text('Masuk'),
                        );
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 15.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Belum punya akun ? '),
                    TextButton(
                      onPressed: _toRegisterScreen,
                      child: Text('daftar'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
