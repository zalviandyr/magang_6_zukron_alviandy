import 'package:book_rent_flutter/blocs/blocs.dart';
import 'package:book_rent_flutter/constants/enum.dart';
import 'package:book_rent_flutter/event_states/event_states.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:book_rent_flutter/ui/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DrawerScreen extends StatefulWidget {
  final DrawerSection section;

  const DrawerScreen({
    Key? key,
    required this.section,
  }) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  late UserBloc _userBloc;
  late BookBloc _bookBloc;
  late List<Map<DrawerSection, Widget>> _screens;
  late DrawerSection _section;

  @override
  void initState() {
    // init bloc
    _userBloc = BlocProvider.of<UserBloc>(context);
    _bookBloc = BlocProvider.of<BookBloc>(context);

    // init screen
    _screens = [];
    _screens
      ..add({DrawerSection.home: HomeScreen()})
      ..add({DrawerSection.myBook: MyBookScreen()})
      ..add({DrawerSection.myRent: MyRentScreen()})
      ..add({DrawerSection.gesture: GestureScreen()});

    // set section
    _section = widget.section;

    // fetch data
    if (_section == DrawerSection.home) {
      _bookBloc.add(BookPublishFetch());
    }

    if (_section == DrawerSection.myBook) {
      _bookBloc.add(BookFetch());
    }

    super.initState();
  }

  void _homeAction() {
    setState(() => _section = DrawerSection.home);
    Navigator.pop(context);

    // fetch book publish
    _bookBloc.add(BookPublishFetch());
  }

  void _myRentAction() {
    setState(() => _section = DrawerSection.myRent);
    Navigator.pop(context);

    // fetch book publish
    _bookBloc.add(BookRentFetch());
  }

  void _myBookAction() {
    setState(() => _section = DrawerSection.myBook);
    Navigator.pop(context);

    // fetch data
    _bookBloc.add(BookFetch());
  }

  void _gestureAction() {
    setState(() => _section = DrawerSection.gesture);

    Navigator.pop(context);
  }

  void _logoutAction() async {
    await _userBloc.userLogout();

    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (_) => LoginScreen()),
      (route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Book Rent')),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25.0),
                  bottomRight: Radius.circular(25.0),
                ),
              ),
              child: Image.asset(
                'images/book.png',
                scale: 0.5,
              ),
            ),
          ]..add(_buildListTile()),
        ),
      ),
      body: Stack(
        children: _screens.map((e) {
          return Offstage(
            offstage: e.keys.first != _section,
            child: e.values.first,
          );
        }).toList(),
      ),
    );
  }

  Widget _buildListTile() {
    return FutureBuilder<User?>(
      future: _userBloc.userLogin(),
      builder: (context, snapshot) {
        List<Widget> children = [];

        if (snapshot.hasData && snapshot.data != null) {
          User user = snapshot.data!;

          if (user.isPeminjam) {
            children = [
              ListTile(
                onTap: _homeAction,
                title: Text('Home'),
              ),
              ListTile(
                onTap: _myRentAction,
                title: Text('Peminjaman Saya'),
              ),
              ListTile(
                onTap: _gestureAction,
                title: Text('Gesture'),
              ),
              ListTile(
                onTap: _logoutAction,
                title: Text(
                  'Log Out',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ];
          }

          if (!user.isPeminjam) {
            children = [
              ListTile(
                onTap: _myBookAction,
                title: Text('Buku Saya'),
              ),
              ListTile(
                onTap: _gestureAction,
                title: Text('Gesture'),
              ),
              ListTile(
                onTap: _logoutAction,
                title: Text(
                  'Log Out',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ];
          }
        }

        return Column(children: children);
      },
    );
  }
}
