import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:book_rent_flutter/constants/url.dart';
import 'package:book_rent_flutter/models/models.dart';

class UserService {
  static Future<bool> register(User user) async {
    Uri url = getUrl('/register');
    var response = await http.post(
      url,
      body: {
        'name': user.name,
        'email': user.email,
        'password': user.password,
        'phone_number': user.phoneNumber,
        'address': user.address,
        'role_id':
            (user.isPeminjam) ? '2' : '1', // id 2 == peminjam, id 1 == pemilik
        'device_id': user.deviceId
      },
    );

    dynamic json = jsonDecode(response.body);
    return json['success'];
  }

  static Future<User?> login(String email, String password) async {
    Uri url = getUrl('/login');
    var response = await http.post(url, body: {
      'email': email,
      'password': password,
    });

    dynamic json = jsonDecode(response.body);
    if (json['success']) {
      return User.fromJson(json['data']);
    } else {
      return null;
    }
  }
}
