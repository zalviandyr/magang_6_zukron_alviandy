import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:book_rent_flutter/constants/url.dart';
import 'package:book_rent_flutter/models/models.dart';

class GenreService {
  static Future<List<Genre>> getGenre(User user) async {
    Uri url = getUrl('/genre');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.get(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    return Genre.fromJsonToList(json['data']);
  }
}
