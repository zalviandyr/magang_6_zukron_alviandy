import 'dart:convert';

import 'package:book_rent_flutter/constants/url.dart';
import 'package:book_rent_flutter/models/models.dart';
import 'package:http/http.dart' as http;

class PeminjamBookService {
  static Future<List<Book>> getAll(User user) async {
    Uri url = getUrl('/peminjam/book');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.get(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    return Book.fromJsonToList(json['data']);
  }

  static Future<void> rent(User user, Book book) async {
    Uri url = getUrl('/peminjam/book/rent/${book.id}');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.patch(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    if (!json['success']) {
      throw new Exception(json['message']);
    }
  }

  static Future<void> returnBook(User user, Book book) async {
    Uri url = getUrl('/peminjam/book/return/${book.id}');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.patch(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    if (!json['success']) {
      throw new Exception(json['message']);
    }
  }

  static Future<List<Book>> getAllBookRent(User user) async {
    Uri url = getUrl('/peminjam/book/rent');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.get(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    return Book.fromJsonToList(json['data']);
  }
}
