import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:book_rent_flutter/constants/url.dart';
import 'package:book_rent_flutter/models/models.dart';

class PemilikBookService {
  static Future<List<Book>> getAll(User user) async {
    Uri url = getUrl('/pemilik/book');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.get(
      url,
      headers: {'Authorization': 'Basic $auth'},
    );

    dynamic json = jsonDecode(response.body);
    return Book.fromJsonToList(json['data']);
  }

  static Future<bool> create(User user, Book book) async {
    Uri url = getUrl('/pemilik/book');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var request = http.MultipartRequest('POST', url);
    request.headers['Authorization'] = 'Basic $auth';
    request.fields['genre_id'] = book.genre!.id.toString();
    request.fields['title'] = book.title!;
    request.fields['price'] = book.price.toString();
    request.fields['is_publish'] = book.isPublish! ? '1' : '0';
    request.files.add(await http.MultipartFile.fromPath('image', book.image!));

    var response = await request.send();
    String responseStr = await response.stream.bytesToString();
    dynamic json = jsonDecode(responseStr);

    return json['success'];
  }

  static Future<bool> update(User user, Book book) async {
    Uri url = getUrl('/pemilik/book/${book.id}');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response = await http.patch(url, headers: {
      'Authorization': 'Basic $auth'
    }, body: {
      'genre_id': book.genre!.id.toString(),
      'title': book.title!,
      'price': book.price!.toString(),
      'is_publish': book.isPublish! ? '1' : '0',
    });

    dynamic json = jsonDecode(response.body);
    return json['success'];
  }

  static Future<bool> delete(User user, Book book) async {
    Uri url = getUrl('/pemilik/book/${book.id}');
    String token = '${user.email}:${user.password}';
    String auth = base64Encode(utf8.encode(token));

    var response =
        await http.delete(url, headers: {'Authorization': 'Basic $auth'});

    dynamic json = jsonDecode(response.body);
    return json['success'];
  }
}
